# Trabaja en ti

Todo comenzó un 7 de marzo del 2023 a raíz de una publicación que mi amigo Gabriel hizo de la siguiente manera: *Trabaja mucho en ti, eres tu proyecto más importante*.

Así que decidí desarrolar una historia que fuera entretenida mientras se aborda este tema tan interesante. 

Inicialmente, me permitió hacer unos comentarios al respecto de dicha publicación porque a mi consideración es falaz.

- Tomando como base la primera oración, se ha demostrado que *trabajar mucho* en cualquier cosa o situación termina siendo perjudicial en la mayoría de ocasiones, más aún, si es por periodos prolongados.   
En mi experiencia y perspectiva prefiero lo siguiente: ***Trabajar un poco en mi mismo, pasar suficiente tiempo conmigo mismo, mirarme cuidadosamente, ¿cómo soy?, ¿cómo me siento?, ¿cómo me pongo pie?, ¿cómo hablo?, ¿qué hago?, ¿qué evito?, etc. me gusta observarme muy cuidadosamente***. Porque estoy consciente que las personas captarán aquello menciono por lo general lo *exagerarán* sobretodo si desde su percepción lo consideran *"malo o negativo"*.  
Por eso es preferible al menos *trabajar en uno mismo para ser de tal manera como nos gustaría ser*, sin niguna garantía de que se logrará y si lo aprueban o desaprueban los demás eso es irrelevante.  
Es poco importante buscar *"ser la mejor versión de uno mismo"*, o *"ser una persona exitosa"* como lo marcan los "estándares de cada época". Lo más importante es que ***al menos nos hemos hecho de tal manera que noa gusta como somos***, eso deberías ser casi una obligación de todo ser humano.

- En el caso se la segunda oración y con base en el primer punto, considerarte como *"un proyecto"* lo más seguro es que terminarán perdidos o frustados, porque se están limitando en algo que nunca serán, ***lo primero es ser un humano conciente y después una persona consciente***.

Finalmente, ***la vida es más importante*** que el ambiente de la vida (tu familia,tu trabajo o lo que haces o dejas de hacer, tu fiesta, etc.). La vida es todo lo que está en tu interior (la realidad), el ambiente de la vida está en el exterior. La vida es lo único que puedes llegar a conocer, lo demás sólo son cosas imaginadas. Estríctamente tú como persona tampoco eres tan importante, sin embargo, ***tú como un instante de vida es de suma importancia*** porque está latiendo, está vivo y eso es todo lo que hay. ***Eso es la base de TODO, el universo existe para ti, sólo porque tú ERES en tu experiencia***.
